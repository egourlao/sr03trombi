<!doctype html>
<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

	if (isset($_GET['nom']) || isset($_GET['prenom'])) {
		$search = true;
		$request = @file_get_contents('https://webapplis.utc.fr/Trombi_ws/mytrombi/result?nom=' . $_GET['nom'] . '&prenom=' . $_GET['prenom']);
		if ($request == FALSE) {
			$error = true;
		} else {
			$results = json_decode($request);
			$error = false;
		}
	} else {
		$search = false;
		$error = false;
	}

?>
<html>
	<head>
		<title>UTC - Trombinoscope</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1>Trombinoscope UTC - SU</h1>
					</div>
				</div>
			</div>
			<?php
				if ($search && !$error) {
					?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							Résultats de la recherche
						</div>
						<div class="panel-body">
							<?php
								$i = 0;
								if (count($results) == 0) {
									echo('<p>Aucun résultat correspondant à la recherche.</p>');
								}
								foreach ($results as $result) {
									if ($i == 0) {
										echo('<div class="row">');
									}
									$i++;
									echo('<div align="center" class="col-sm-4">');
									if ($result->autorisation == 'O') {
										echo('<img class="img-thumbnail" onerror="this.src=\'default.png\'" src="https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur_mini?username=' . $result->login . '">');
									} else {
										echo('<img class="img-thumbnail" src="default.png">');
									}
									echo('<p><a href="mailto:' . $result->mail . '">' . $result->nom . '</a>');
									echo('<br />' . $result->structure);
									if(isset($result->sousStructure) && strlen($result->sousStructure)) {
										echo('<br />' . $result->sousStructure);
									}
									if(isset($result->poste) && strlen($result->poste)) {
										echo('<br />' . $result->poste);
									}
									if(isset($result->bureau) && strlen($result->bureau)) {
										echo('<br /><br /><strong>Bureau</strong> : ' . $result->bureau);
									}
									if(isset($result->tel) && strlen($result->tel)) {
										echo('<br /><strong>Téléphone</strong> : ' . $result->tel);
									}
									if(isset($result->tel2) && strlen($result->tel2)) {
										echo('<br /><strong>Téléphone secondaire</strong> : ' . $result->tel2);
									}

									echo('</p></div>');
									if ($i == 3) {
										echo('</div>');
										$i = 0;
									}
									?>
								<?php
									}
								if ($i != 0) {
									echo("</div>");
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
				} else if ($error) {
					?>
					<script>
						document.addEventListener('DOMContentLoaded', function() {
							$.notify({
								message: 'Impossible d\'accéder au serveur.'
							}, {
								type: 'danger',
								delay: 1000
							});
						}, false);
					</script>
					<?php
				}
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Formulaire
						</div>
						<div class="panel-body">
							<form method="GET" id="search-form">
								<div class="form-group col-md-6">
									<label>Nom</label>
									<?php
										if (isset($_GET['nom'])) {
											echo('<input type="text" class="form-control" placeholder="Martinet" id="nom" name="nom" value="' . $_GET['nom'] . '" />');
										} else {
											echo('<input type="text" class="form-control" placeholder="Martinet" id="nom" name="nom" />');
										}
									?>
								</div>
								<div class="form-group col-md-6">
									<label>Prénom</label>
									<?php
										if (isset($_GET['prenom'])) {
											echo('<input type="text" class="form-control" placeholder="Cédric" id="prenom" name="prenom" value="' . $_GET['prenom'] . '" />');
										} else {
											echo('<input type="text" class="form-control" placeholder="Cédric" id="prenom" name="prenom" />');
										}
									?>
								</div>
								<button id="search-button" class="btn btn-primary">Rechercher</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://rawgit.com/mouse0270/bootstrap-notify/master/bootstrap-notify.min.js"></script>
	<script>
		$('#search-form').on('submit', function(e) {
			if ($('#nom').val().length === 0 && $('#prenom').val().length === 0) {
				$.notify({
					message: 'Veuillez remplir au moins un des champs de recherche.'
				}, {
					type: 'danger',
					delay: 1000
				});
				e.preventDefault();
			}
		});
	</script>
</html>
