<!doctype html>
<html>
	<head>
		<title>UTC - Trombinoscope</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1>Trombinoscope</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Résultats de la recherche
						</div>
						<div class="panel-body">
							<form method="GET" id="search-form" action="trombi.php">
								<div class="form-group col-md-6">
									<label>Nom</label>
									<input type="text" class="form-control" placeholder="Martinet" id="nom" />
								</div>
								<div class="form-group col-md-6">
									<label>Prénom</label>
									<input type="text" class="form-control" placeholder="Cédric" id="prenom" />
								</div>
								<button id="search-button" class="btn btn-primary">Rechercher</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://rawgit.com/mouse0270/bootstrap-notify/master/bootstrap-notify.min.js"></script>
	<script>

	</script>
</html>
