<?php

if (!isset($_GET['struct']) || $_GET['struct'] == '') {
	http_response_code(400);
} else {
	$result = @file_get_contents('https://webapplis.utc.fr/Trombi_ws/mytrombi/structfils?lid=' . $_GET['struct']);
	$result_array = json_decode($result);
	if (count($result_array) == 0) {
		http_response_code(404);
	}
	header('Content-Type: application/json');
	echo $result;
}
