<!doctype html>
<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

	$struct_request = @file_get_contents('https://webapplis.utc.fr/Trombi_ws/mytrombi/structpere');
	$struct_results = json_decode($struct_request);

	if (isset($_GET['nom']) || isset($_GET['prenom'])) {
		$url = 'https://webapplis.utc.fr/Trombi_ws/mytrombi/result?nom=' . $_GET['nom'] . '&prenom=' . $_GET['prenom'];
	} else if (isset($_GET['struct']) && $_GET['struct'] != '') {
		$url = 'https://webapplis.utc.fr/Trombi_ws/mytrombi/resultstruct?pere=' . $_GET['struct'];
		if (isset($_GET['sousstruct']) && $_GET['sousstruct'] != '') {
			$url .= '&fils=' . $_GET['sousstruct'];
		} else {
			$url .= '&fils=0';
		}
	} else {
		$search = false;
		$error = false;
	}

	if(isset($url)) {
		$search = true;
		$request = @file_get_contents($url);
		if ($request == FALSE) {
			$error = true;
		} else {
			$results = json_decode($request);
			$error = false;
		}
	}

?>
<html>
	<head>
		<title>UTC - Trombinoscope</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1><a href="index4.php">Trombinoscope UTC - SU</a></h1>
					</div>
				</div>
			</div>
			<?php

				if ($search && !$error) {
					if (!count($results)) {
						echo('<div class="row">');
					} else {
						echo('<div class="row" style="margin-bottom:37px;">');
					}
					?>
				<div class="col-md-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							Résultats de la recherche
						</div>
						<div class="panel-body">
							<?php
								$i = 0;
								$firstletters = array();
								if (count($results) == 0) {
									echo('<p>Aucun résultat correspondant à la recherche.</p>');
								}
								foreach ($results as $result) {
									if ($i == 0) {
										echo('<div class="row">');
									}
									$i++;
									if (!isset($firstletters[$result->nom[0]])) {
										$firstletters[$result->nom[0]] = true;
										echo('<div align="center" id="'  . $result->nom[0] . '" class="col-sm-4">');
									} else {
										echo('<div align="center" class="col-sm-4">');
									}
									if ($result->autorisation == 'O') {
										echo('<img class="img-thumbnail" onerror="this.src=\'default.png\'" src="https://demeter.utc.fr/portal/pls/portal30/portal30.get_photo_utilisateur_mini?username=' . $result->login . '">');
									} else {
										echo('<img class="img-thumbnail" src="default.png">');
									}
									echo('<p><a href="mailto:' . $result->mail . '">' . $result->nom . '</a>');
									echo('<br />' . $result->structure);
									if(isset($result->sousStructure) && strlen($result->sousStructure)) {
										echo('<br />' . $result->sousStructure);
									}
									echo('<span class="more-info">');
									if(isset($result->poste) && strlen($result->poste)) {
										echo('<br />' . $result->poste);
									}
									if(isset($result->bureau) && strlen($result->bureau)) {
										echo('<br /><br /><strong>Bureau</strong> : ' . $result->bureau);
									}
									if(isset($result->tel) && strlen($result->tel)) {
										echo('<br /><strong>Téléphone</strong> : ' . $result->tel);
									}
									if(isset($result->tel2) && strlen($result->tel2)) {
										echo('<br /><strong>Téléphone secondaire</strong> : ' . $result->tel2);
									}
									echo('</span>');
									if ((isset($result->poste) && strlen($result->poste)) || (isset($result->bureau) && strlen($result->bureau)) || (isset($result->tel) && strlen($result->tel)) || isset($result->tel2) && strlen($result->tel2)) {
										echo('<br /><button class="btn btn-primary show glyphicon glyphicon-eye-open"></button>');
									}

									echo('</p></div>');
									if ($i == 3) {
										echo('</div>');
										$i = 0;
									}
									?>
								<?php
									}
								if ($i != 0) {
									echo("</div>");
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
				} else if ($error) {
					?>
					<script>
						document.addEventListener('DOMContentLoaded', function() {
							$.notify({
								message: 'Impossible d\'accéder au serveur.'
							}, {
								type: 'danger',
								delay: 1000
							});
						}, false);
					</script>
					<?php
				}
			?>
			<?php
				if ($search && count($results)) {
			?>
			<div class="navbar navbar-default navbar-fixed-bottom">
				<div class="container">
					<ul class="navbar-nav nav">
						<?php
							foreach (array_keys($firstletters) as $firstletter) {
								echo '<li style="padding:2px"><a style="font-size: 9px;padding:15px 2px" href="#' . $firstletter . '">' . $firstletter . '</a></li>';
							}
						?>
					</ul>
					<div class="navbar-right">
						<form method="GET" class="navbar-form mini-navbar-search-form" id="search-form" <?php if (isset($_GET['struct'])) { echo 'style="display:none;"'; } ?> >
							<div class="form-group">
								<label>Nom</label>
								<?php
									if (isset($_GET['nom'])) {
										echo('<input type="text" class="form-control" placeholder="Nom" id="nom" name="nom" value="' . $_GET['nom'] . '" />');
									} else {
										echo('<input type="text" class="form-control" placeholder="Nom" id="nom" name="nom" />');
									}
								?>
							</div>
							<div class="form-group">
								<label>Prénom</label>
								<?php
									if (isset($_GET['prenom'])) {
										echo('<input type="text" class="form-control" placeholder="Prénom" id="prenom" name="prenom" value="' . $_GET['prenom'] . '" />');
									} else {
										echo('<input type="text" class="form-control" placeholder="Prénom" id="prenom" name="prenom" />');
									}
								?>
							</div>
							<button id="search-button" class="btn btn-primary">Rechercher</button>
							<button class="btn btn-success change-btn glyphicon glyphicon-refresh"></button>
						</form>
						<form method="GET" id="search-struct-form" class="navbar-form mini-navbar-search-form" <?php if (!isset($_GET['struct'])) { echo 'style="display:none;"'; } ?> >
							<div class="form-group">
								<label>Structure</label>
								<select class="form-control" id="struct" name="struct">
									<option value="nostruct">--</option>
									<?php
										foreach($struct_results as $struct_available) {
											echo '<option value="' . $struct_available->structure->structId . '">' . $struct_available->structureLibelle . '</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Sous-structure</label>
								<select class="form-control" id="sousstruct" name="sousstruct">
									<option value="0">--</option>
								</select>
							</div>
							<button id="search-struct-button" class="btn btn-primary">Rechercher</button>
							<button class="btn btn-success change-btn glyphicon glyphicon-refresh"></button>
						</form>
					</div>
				</div>
			</div>
			<?php
				} else {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Recherche par nom
						</div>
						<div class="panel-body">
							<form method="GET" id="search-form">
								<div class="form-group col-md-6">
									<label>Nom</label>
									<?php
										if (isset($_GET['nom'])) {
											echo('<input type="text" class="form-control" placeholder="Martinet" id="nom" name="nom" value="' . $_GET['nom'] . '" />');
										} else {
											echo('<input type="text" class="form-control" placeholder="Martinet" id="nom" name="nom" />');
										}
									?>
								</div>
								<div class="form-group col-md-6">
									<label>Prénom</label>
									<?php
										if (isset($_GET['prenom'])) {
											echo('<input type="text" class="form-control" placeholder="Cédric" id="prenom" name="prenom" value="' . $_GET['prenom'] . '" />');
										} else {
											echo('<input type="text" class="form-control" placeholder="Cédric" id="prenom" name="prenom" />');
										}
									?>
								</div>
								<button id="search-button" class="btn btn-primary">Rechercher par nom</button>
							</form>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Recherche par structure
						</div>
						<div class="panel-body">
							<form method="GET" id="search-struct-form">
								<div class="form-group col-md-6">
									<label>Structure</label>
									<select class="form-control" id="struct" name="struct">
										<option value="nostruct">--</option>
										<?php
											foreach($struct_results as $struct_available) {
												echo '<option value="' . $struct_available->structure->structId . '">' . $struct_available->structureLibelle . '</option>';
											}
										?>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label>Sous-structure</label>
									<select class="form-control" id="sousstruct" name="sousstruct">
										<option value="0">--</option>
									</select>
								</div>
								<button id="search-struct-button" class="btn btn-primary">Rechercher par structure</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://rawgit.com/mouse0270/bootstrap-notify/master/bootstrap-notify.min.js"></script>
	<script>
		$('#search-form').on('submit', function(e) {
			if ($('#nom').val().length === 0 && $('#prenom').val().length === 0) {
				$.notify({
					message: 'Veuillez remplir au moins un des champs de recherche.'
				}, {
					type: 'danger',
					delay: 1000
				});
				e.preventDefault();
			} else if ($('#nom').val().length < 2 && $('#prenom').val().length < 2) {
				$.notify({
					message: 'Veuillez remplir au moins deux caractères dans un des champs de recherche.'
				}, {
					type: 'danger',
					delay: 1000
				});
				e.preventDefault();
			}
		});

		$('.show').click(function(e) {
			e.preventDefault();
			$(e.target).parent().find('.more-info').toggle();
			if ($(e.target).hasClass('glyphicon-eye-open')) {
				$(e.target).addClass('glyphicon-eye-close');
				$(e.target).removeClass('glyphicon-eye-open');
			} else {
				$(e.target).addClass('glyphicon-eye-open');
				$(e.target).removeClass('glyphicon-eye-close');
			}
		});
		$('.more-info').toggle();

		$('.change-btn').click(function(e) {
			e.preventDefault();
			$('.mini-navbar-search-form').toggle();
		});

		$('#struct').change(function(e) {
			$('.sousstruct-choice').remove();
			if ($(e.target).val() === 'nostruct') {
				return;
			}
			$.ajax('struct_wrapper.php?struct=' + $(e.target).val()).done(function(response) {
				response.forEach(function(sStruct) {
					var opt = $(document.createElement('option')).attr('value', sStruct.structure.structId).text(sStruct.structureLibelle);
					opt.addClass('sousstruct-choice');
					$('#sousstruct').append(opt);
				});
			});
		});
	</script>
</html>
